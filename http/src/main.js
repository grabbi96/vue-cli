import Vue from 'vue';
import VueResource from 'vue-resource';
import App from './App.vue';
import { request } from 'https';

Vue.use(VueResource);

Vue.http.options.root = 'https://my-prac-cf5cf.firebaseio.com/data.json';
Vue.http.interceptors.push((request, next) =>{
  console.log(request);
  if(request.method == 'POST'){
    request.method = 'POST';
  }
  next();
})


new Vue({
  el: '#app',
  render: h => h(App)
})
