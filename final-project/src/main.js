import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource';


import App from './App.vue';
import store from './store/store';


import  { routes } from './routes.js';


Vue.filter('currency', (value) => {
  return '$'+ value.toLocaleString();
})
const router = new VueRouter({
  mode:'history',
  routes
});




Vue.use(VueRouter);
Vue.use(VueResource)

Vue.http.options.root = 'https://vue-stock-trader-9ebf7.firebaseio.com/';

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
