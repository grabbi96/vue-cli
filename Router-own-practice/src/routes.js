import Home from './component/Home.vue';
import About from './component/About.vue';
import Service from './component/Service.vue';
import Team from './component/Team.vue';
import TeamStart from './component/TeamStart.vue';
import TeamDetails from './component/TeamDetails.vue';
import TeamEdit from './component/TeamEdit.vue';
import Button from './component/Button.vue';

export const routes = [
  {path:'', name:'home', component:Home},
  {path:'/about', name:'about', components:{ default:About, 'app-button':Button }},
  {path:'/service', components:{
    default:Service, 'app-button':Button
    }},
  {path:'/team', components:{default:Team, 'app-button': Button}, children:[
      {path:'/', component:TeamStart},
      {path:':id', component:TeamDetails},
      {path:':id/edit', component:TeamEdit, name:'TeamUserEdit'}
    ]},
  {path:'/redirect', redirect:'/'},
  {path:'*', redirect:'/'}


]
