export  const fruitMixid = {
  data(){
    return {
      fruits:['mango', 'banana', 'apple', 'melon'],
      filterValues:''
    }
  },
  computed:{
    filderedFruits(){
      return this.fruits.filter((element)=> {
        return element.match(this.filterValues);
      })
    }
  }
}
