const cafeList = document.querySelector('#cafe-list');
const addCafeList = document.querySelector("#add-cafe-form");

//show data from firebase
function renderCafe(ele){
    let li = document.createElement('li');
    let name = document.createElement('span');
    let city = document.createElement('span');
    let cross = document.createElement('div');

    li.setAttribute('data-id', ele.id);
    name.textContent = ele.data().name;
    city.textContent = ele.data().city;
    cross.textContent = 'x';

    li.className = "list-group-item";
    li.appendChild(name);
    li.appendChild(city);
    li.appendChild(cross);

    cafeList.appendChild(li);

    cross.addEventListener('click', (e) => {
        e.stopPropagation();
        let id = e.target.parentElement.getAttribute('data-id');
        db.collection('cafes').doc(id).delete();
    })


}


//getting data
// // db.collection('cafes').where('city', '==', 'dhaka').get().then(snapshot => {
//     console.log(typeof snapshot);
//     snapshot.docs.forEach(element => {
//         renderCafe(element);
//     });
// });


//saving data
addCafeList.addEventListener('submit', (e) => {
    e.preventDefault();
    db.collection('cafes').add({
        name: addCafeList.name.value,
        city: addCafeList.city.value
    })
    console.log(addCafeList.name.value);
    addCafeList.city.value = "";
    addCafeList.name.value = "";
});

//real-time data

db.collection('cafes').orderBy('city').onSnapshot(snapshot => {
    let changes = snapshot.docChanges();
    changes.forEach(change => {
        if(change.type == 'added'){
            renderCafe(change.doc); 
        }else if(change.type == 'removed'){
            let li = cafeList.querySelector('[data-id='+ change.doc.id +']');
            cafeList.removeChild(li);
        }
    });
})
