import Vue from 'vue'
import App from './App.vue';
import axios from 'axios';

import router from './router'
import store from './store'

axios.defaults.baseURL = 'https://vue-http-a12eb.firebaseio.com/';
axios.defaults.headers.common['Authorization'] = 'asdfasd';
axios.defaults.headers.get['Accepts'] = 'Applications/json';

const reqinterceptor = axios.interceptors.request.use(res => {
  console.log('Interceptors request', res);
  return res;
})
const resinterceptor = axios.interceptors.response.use(res => {
  console.log('Interceptors response', res);
  return res;
})

axios.interceptors.request.eject(reqinterceptor);
axios.interceptors.response.eject(resinterceptor);

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
