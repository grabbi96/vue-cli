
const state = {
    counter: 0,

  };
const getters = {
    counter: state =>{
      return state.counter;
    },
    doubleCounter: state => {
      return state.counter * 2;
    },
    stringCounter: state => {
      return state.counter + ' string';
    }
  };

const mutations ={
    increment: (state, payload) => {
      state.counter += payload;
    },
    decrement: (state, payload) => {
      state.counter -= payload;
    }
  };
const actions ={
    increment: ({
      commit
    }, payload) => {
      commit('increment', payload);
    },

    decrement: ({
      commit
    }, payload) => {
      commit('decrement', payload);
    },



    asyncDecrement: ({commit}, payload) => {
      setTimeout(() => {
        commit('decrement', payload);
      }, 1000)
    },
    asyncincrement: ({commit}, payload) => {
      setTimeout(() => {
        commit('increment', payload);
      }, 1000)

    }
  };

  export default{
    state,
    getters,
    mutations,
    actions

  }
