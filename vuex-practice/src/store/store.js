import Vue from 'vue';
import Vuex from 'vuex';


Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    value:2,
    todos: [
      {
        task: 'Code',
        completed: 12
      },
      {
        task: 'Sleep',
        completed: 25
      },
      {
        task: 'Eat',
        completed: 35
      }
    ]
  },
  getters:{
    afterValue: state => state.value * 2,
    todos: state => state.todos
  },
  mutations: {
    increment:(state, payload) =>{
      state.value += payload;
    },
    deleteItem:(state, payload) =>{
      state.todos.splice(payload.index, payload.count);
    },
    addItem:(state, payload) =>{
      state.todos.push(payload);
    },
    todoTask:(state, payload) =>{
      state.todos.push(payload);
    }
  }
})
