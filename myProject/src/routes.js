import Home from './components/Home.vue';
import About from './components/About.vue';
import Header from './components/header/Header.vue';
import Product from './components/product/Product.vue';
import Productstart from './components/product/products/Productstart.vue';
import Productlist from './components/product/products/Productlist.vue';
import Dashbroad from './components/dashbroad/Dashbroad.vue';
import Items from './components/dashbroad/dashitem/Items.vue';
import ItemGroup from './components/dashbroad/dashitem/ItemGroup.vue';
import ItemCat from './components/dashbroad/dashitem/ItemCat.vue';
import DashHome from './components/dashbroad/dashitem/DashHome.vue';
import ProductDetails from './/components/product/products/ProductDetails.vue'

export const routes = [
  {path:'/', components:{default:Home, 'app-header':Header}},
  {path:'/about', components:{default:About, 'app-header':Header}},
  {path:'/dashbroad', component:Dashbroad, children:[
    {path:'/', component:DashHome},
    {path:'/items', component:Items},
      {path:'/itemgroup', component:ItemGroup},
      {path:'/itemcat', component:ItemCat}
  ]},
  {
    path:'/product', components:{default:Product, 'app-header': Header}, children:[
      {path:'', component:Productstart},
      {path:':id', component:Productlist},
      {path:':id/itemdetails', component:ProductDetails, name:'itemdetail'}
    ]
  }
]
