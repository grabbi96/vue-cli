import Vue from 'vue';
import Vuex from 'vuex';
// import setdata from './setdata';
import axios from 'axios';
Vue.use(Vuex);
export const store = new Vuex.Store({
  state:{
      allItems:[
        {title:'nokia',
        category:'electronics',
        group:'two',
        price:120,
        description:'this is going one',
        img:'https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/img (55).jpg'},


        {title:'pants',
        category:'clothes',
        group:'one',
        price:10,
        description:'this is going one',
        img:'https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Vertical/12.jpg'}
      ],
      mainCategory:[
        {img:'https://mdbootstrap.com/img/Photos/Others/food.jpg',
        name:'electronics',
        itemQuantity:4,
        details:'this is one item'},

        {img:'https://mdbootstrap.com/img/Photos/Lightbox/Thumbnail/img%20(147).jpg',
        name:'clothes',
        itemQuantity:5,
        details:'this is two item'},

        ],
      groups:[

      ],
      cartItems:[],
      totalPrice:0

  },
  getters:{
    allItems:state =>{
      return state.allItems;
    },
    mainCategory:state =>{
      return state.mainCategory;
    },
    group:state =>{
      return state.groups;
    },
    cartItems:state => {
      return state.cartItems
    },
    totalPrice:state =>{
       let aa = 0;
       state.cartItems.forEach(element =>{
         aa += element.itemprice
       })
       state.totalPrice = aa
       return state.totalPrice;
    },
    // allData:getters =>{

    //   console.log(getters.totalPrice)
    //   return allData;
    // }
  },
  mutations:{
    'ADD_CATEGORY' (state, payload){
      state.mainCategory.push(payload);
      console.log(state.mainCategory);
    },
    'ADD_GROUP' (state, payload){
      state.groups.push(payload);
      console.log(state.groups);
    },
    'ADD_Item' (state, payload){
      state.allItems.push(payload);
      console.log(state.groups);
    },
    'DELETE_CAT'(state, payload){
      state.mainCategory.splice(payload, 1);
    },
    'DELETE_GROUP'(state, payload){
      state.groups.splice(payload, 1);
    },
    'DELETE_ITEM'(state, payload){
      state.allItems.splice(payload, 1);
    },
    'UPDATE_CAT'(state, payload){
      state.mainCategory[payload.ic] = payload.updateCategory;
    },
    'UPDATE_GROUP'(state, payload){
      state.groups[payload.ic] = payload.updateGroup;
    },
    'UPDATE_ITEM'(state, payload){
      state.allItems[payload.ic] = payload.updateItem;
    },
    'ADDITEM_CART'(state, payload){
      payload.itemprice = (payload.price * Number(payload.quantity))
      const record = state.cartItems.find(element => {
        return element.name == payload.name
      });
      if(record){
        record.quantity += payload.quantity;
        record.itemprice = (record.quantity * Number(record.price))
      }else{
        state.cartItems.push(payload);
      }
    },
    "MULTIPLY_PRICE"(state){
      state.cartItems.forEach(element =>{
        element.itemPrice = element.price * element.quantity;
      })
    },
    "INCREASE_ITEM"(state, {index, quan, price}){
      state.cartItems[index].quantity += quan;
      state.cartItems[index].itemprice = state.cartItems[index].quantity * price;
    },
    'DISCREASE_ITEM'(state, {index, quan, price}){
      state.cartItems[index].quantity -= quan;
      state.cartItems[index].itemprice = state.cartItems[index].quantity * price;

  },
    'DELETECART_ITEM'(state, payload){
      state.cartItems.splice(payload, 1);
    },
    'SET_DATA'(state){
      let allData = {
        allItems:state.allItems,
        mainCategory:state.mainCategory,
        groups:state.groups,
        cartItem:state.cartItems,
        totalPrice:store['getters'].totalPrice
      }
      console.log(allData);
      axios.put('https://my-prac-cf5cf.firebaseio.com/allData.json', allData);
    },
    'GET_DATA'(state, payload){
      state.allItems = payload.allItems;
      state.cartItems = payload.cartItem;
      state.mainCategory = payload.mainCategory;
      state.groups = payload.groups;
      state.totalPrice = payload.totalPrice;
      console.log(payload);
    }

  },
  actions:{
    addCategory({commit}, payload){
      commit('ADD_CATEGORY', payload);
    },
    addGroup({commit}, payload){
      commit('ADD_GROUP', payload);
    },
    addItem({commit}, payload){
      commit('ADD_Item', payload);
    },
    deleteCat({commit}, payload){
      commit('DELETE_CAT', payload);
    },
    deleteGroup({commit}, payload){
      commit('DELETE_GROUP', payload);
    },
    deleteItem({commit}, payload){
      commit('DELETE_ITEM', payload);
    },
    updateCat({commit}, payload){
      commit('UPDATE_CAT', payload);
    },
    updateGroup({commit}, payload){
      commit('UPDATE_GROUP', payload);
    },
    updateItem({commit}, payload){
      commit('UPDATE_ITEM', payload);
    },
    addCartItem({commit}, payload){
      commit('ADDITEM_CART', payload);
    },
    multiItem({commit}, payload){

    },
    increaseItem({commit}, payload){
      commit("INCREASE_ITEM", payload);
    },
    discreaseItem({commit}, payload){
      commit('DISCREASE_ITEM', payload);
    },
    deleteCartItem({commit}, payload){
      commit('DELETECART_ITEM', payload);
    },
    setData({commit}){
      commit('SET_DATA');
    },
    getData({commit}){
      axios.get('https://my-prac-cf5cf.firebaseio.com/allData.json').then(response => {
        commit('GET_DATA', response.data)
      });
    }
  },
  modules:{

  }
})
