
import HelloWorld from '../components/HelloWorld.vue';
import Login from '../components/pages/login.vue';


export const routes =[
    {path:'/', name:'Helloworld', redirect:{path:'login'}},
    {path:'/login', name:'login', component:Login}
  ]

